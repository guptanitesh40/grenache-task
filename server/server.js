'use strict';

const { PeerRPCServer } = require("grenache-nodejs-ws");
const Link = require("grenache-nodejs-link");

const link = new Link({
    grape: "http://127.0.0.1:30001"
});

link.start();

const peer = new PeerRPCServer(link, {});
peer.init();

const port = 1337;
const service = peer.transport('server')
service.listen(port)

const orderBook = [];

setInterval(() => {
    link.announce('addOrders', service.port, {})
}, 1000)


service.on('request', (rid, key, payload, handler) => {

    if(key === "addOrders") {
        payload.data.map(element => {
        
            let isOrderExists = false;
            for(let i = 0; i < orderBook.length; i++) {
    
                if(orderBook[i]?.orderId === element.orderId) {
                    isOrderExists = true;
                    break;
                }
            }
            if(!isOrderExists) {
                orderBook.push(element);
            }
        });
        console.log(orderBook);
        handler.reply(null, { data: orderBook });
    }


});


