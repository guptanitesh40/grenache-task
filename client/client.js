'use strict';

const { PeerRPCClient } = require("grenache-nodejs-ws");
const Link = require("grenache-nodejs-link");

const link = new Link({
    grape: "http://127.0.0.1:30001"
});

link.start();

const peer = new PeerRPCClient(link, {});
peer.init();

const orders = [
    {
        orderId: 1,
        price: 100,
    },
    {
        orderId: 3,
        price: 300,
    }
];
const payload = { data: orders };
peer.request("addOrders", payload, { timeout: 1000000 }, (error, data) => {

    if(error) {
        console.error(error);
        process.exit(-1);
    }

    if(data) {
        data.data.map(element => {
        
            let isOrderExists = false;
            for(let i = 0; i < orders.length; i++) {
    
                if(orders[i]?.orderId === element.orderId) {
                    isOrderExists = true;
                    break;
                }
            }
            if(!isOrderExists) {
                orders.push(element);
            }
        });
        console.log(orders);
    }

});

peer.on('request', (rid, key, payload, handler) => {
    console.log(payload);

});
